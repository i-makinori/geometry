// This application shows balls bouncing on a checkerboard, with no respect
// for the laws of Newtonian Mechanics.  There's a little spotlight to make
// the animation interesting, and arrow keys move the camera for even more
// fun.

#ifdef __APPLE_CC__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <cmath>

// Colors
GLfloat WHITE[] = {1, 1, 1};
GLfloat RED[] = {1, 0, 0};
GLfloat GREEN[] = {0, 1, 0};
GLfloat MAGENTA[] = {1, 0, 1};

// A camera.  It moves horizontally in a circle centered at the origin of
// radius 10.  It moves vertically straight up and down.

class Camera {
  double theta;      // determines the x and z positions
  double y;          // the current y position
  double dTheta;     // increment in theta for swinging the camera around
  double dy;         // increment in y for moving the camera up/down
  double dist_0;
public:
  Camera(): theta(0), y(3), dTheta(0.04), dy(0.2) {
  }
  double getX() {return 10 * cos(theta);}
  double getY() {return y ;}
  double getZ() {return 10 * sin(theta);}
  void moveRight() {theta += dTheta;}
  void moveLeft() {theta -= dTheta;}
  void moveUp() {if (y < 100) y += dy + abs(y*0.05);}
  //void moveDown() {if (y > dy) y -= dy;}
  void moveDown() { if (y>-100) y -= dy + abs(y*0.05);}
};

// A ball.  A ball has a radius, a color, and bounces up and down between
// a maximum height and the xz plane.  Therefore its x and z coordinates
// are fixed.  It uses a lame bouncing algorithm, simply moving up or
// down by 0.05 units at each frame.
class Ball {
  double radius;
  GLfloat* color;
  double maximumHeight;
  double x;
  double y;
  double z;
  int direction;
public:
  Ball(double r, GLfloat* c, double h, double x, double z):
      radius(r), color(c), maximumHeight(h), direction(-1),
      y(h), x(x), z(z) {
  }
  void update() {
    y += direction * 0.05;
    if (y > maximumHeight) {
      y = maximumHeight; direction = -1;
    } else if (y < radius) {
      y = radius; direction = 1;
    }
    glPushMatrix();
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
    glTranslated(x, y, z);
    glutSolidSphere(radius, 30, 30);
    glPopMatrix();
  }

};

// A checkerboard class.  A checkerboard has alternating red and white
// squares.  The number of squares is set in the constructor.  Each square
// is 1 x 1.  One corner of the board is (0, 0) and the board stretches out
// along positive x and positive z.  It rests on the xz plane.  I put a
// spotlight at (4, 3, 7).
class Checkerboard {
  int displayListId;
  int width;
  int depth;
public:
  Checkerboard(int width, int depth): width(width), depth(depth) {}
  double centerx() {return width / 2;}
  double centerz() {return depth / 2;}
  void create() {
    displayListId = glGenLists(1);
    glNewList(displayListId, GL_COMPILE);
    GLfloat lightPosition[] = {4, 3, 7, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glBegin(GL_QUADS);

    glNormal3d(0, 1, 0);
    for (int x = 0; x < width - 1; x++) {
      for (int z = 0; z < depth - 1; z++) {
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE,
                     (x + z) % 2 == 0 ? RED : WHITE);
        glVertex3d(x, 0, z);
        glVertex3d(x+1, 0, z);
        glVertex3d(x+1, 0, z+1);
        glVertex3d(x, 0, z+1);
      }
    }
    glEnd();
    glEndList();
  }
  void draw() {
    glCallList(displayListId);
  }
};

// Global variables: a camera, a checkerboard and some balls.
Checkerboard checkerboard(8, 8);
Camera camera;
Ball balls[] = {
  Ball(1, GREEN, 7, 6, 1),
  Ball(1.5, MAGENTA, 6, 3, 4),
  Ball(0.4, WHITE, 5, 1, 7),
  Ball(0.2, WHITE, 10, 1, 7),
};


// Application-specific initialization: Set up global lighting parameters
// and create display lists.
void init() {
  glEnable(GL_DEPTH_TEST);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, WHITE);
  glLightfv(GL_LIGHT0, GL_SPECULAR, WHITE);
  glMaterialfv(GL_FRONT, GL_SPECULAR, WHITE);
  glMaterialf(GL_FRONT, GL_SHININESS, 30);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  checkerboard.create();
}


void push_to_screen_procedures(float virtual_width, float virtual_height) {
  glDisable(GL_LIGHTING);
  // parallel projection
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0, virtual_width, virtual_height, 0);
  //
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
}

void pop_from_screen_procedures(){
  // collection
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  //
  glEnable(GL_LIGHTING);
}
  

#include <string>


static void DrawString(std::string str, float virtual_width, float virtual_height, float x0, float y0)
{

  push_to_screen_procedures(virtual_width, virtual_height);
  // draw text to screen
  glRasterPos2f(x0, y0);
  //int size = strlen(str);
  int size=str.length();
  for(int i = 0; i < size; i++){   
    //glColor3d(1,1,1);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, str[i]);
    //glLogicOp(GL_SET);
  }
  pop_from_screen_procedures();
}



#include <cstdio>
// Draws one frame, the checkerboard then the balls, from the current camera
// position.
void display() {
  
  GLint w = glutGet(GLUT_WINDOW_WIDTH);
  GLint h = glutGet(GLUT_WINDOW_HEIGHT);
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
            checkerboard.centerx(), 0.0, checkerboard.centerz(),
            0.0, 1.0, 0.0);

  std::string str1;
  
  checkerboard.draw();
  for (int i = 0; i < sizeof(balls) / sizeof(Ball); i++){ //sizeof(Ball); i++) {
    str1 = "iter is" + std::to_string(i);
    balls[i].update();
    DrawString(str1, w, h ,150+i*20,200);
  }

  // axes are ten units long.
  glBegin(GL_LINES);
    glColor3f(1, 0, 0); glVertex3f(0, -10, 0); glVertex3f(10, 0, 0);
    glColor3f(0, 1, 0); glVertex3f(0, -10, 0); glVertex3f(0, 10, 0);
    glColor3f(0, 0, 1); glVertex3f(0, -10, 0); glVertex3f(0, 0, 10);
  glEnd();

  //
  glColor3f(1, 1, 1);
  
  push_to_screen_procedures(h, w); {
    glBegin(GL_POLYGON);
    glVertex2f(10, 10);
    glVertex2f(50, 10);
    glVertex2f(50, 50);
    glVertex2f(10, 50);
    glEnd();
  } pop_from_screen_procedures();

  //
  DrawString("hogehoge", 1,1, 0.05, 0.05);
  
  std::string text = "wid:" + std::to_string(10) + "hei:" + std::to_string(100);
  DrawString(text, w, h ,100,500);
  glFlush();
  glutSwapBuffers();

}

// On reshape, constructs a camera that perfectly fits the window.
void reshape(GLint w, GLint h) {

  glViewport(10, 10, w-20, h-20); // 10px border

  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);

  //glViewport(0, 0, w, h);
  
  glMatrixMode(GL_MODELVIEW);
}

// Requests to draw the next frame.
void timer(int v) {
  glutPostRedisplay();
  glutTimerFunc(1000/60, timer, v);
}

// Moves the camera according to the key pressed, then ask to refresh the
// display.
void special(int key, int, int) {
  switch (key) {
    case GLUT_KEY_LEFT: camera.moveLeft(); break;
    case GLUT_KEY_RIGHT: camera.moveRight(); break;
    case GLUT_KEY_UP: camera.moveUp(); break;
    case GLUT_KEY_DOWN: camera.moveDown(); break;
  }
  glutPostRedisplay();
}

// Initializes GLUT and enters the main loop.
int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowPosition(80, 80);
  glutInitWindowSize(800, 600);
  glutCreateWindow("Bouncing Balls");
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutSpecialFunc(special);
  glutTimerFunc(100, timer, 0);
  init();
  glutMainLoop();
}
