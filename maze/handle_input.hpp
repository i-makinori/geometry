#ifndef HANDLE_INPUT_HPP
#define HANDLE_INPUT_HPP

#include "camera.hpp"
#include "init_app.hpp"

//

extern int mouse_moving;
extern float mouse_x_start;
extern float mouse_y_start;
extern float mouse_x_ago;
extern float mouse_y_ago;
extern float mouse_x_delta;
extern float mouse_y_delta;


// On reshape, constructs a camera that perfectly fits the window.
void reshape(GLint w, GLint h);

// Moves the camera according to the key pressed, then ask to refresh the
// display.
void specialkey_callback(int key, int, int);

void keyboard_callback(unsigned char key, int x, int y);


int truncate_almost_zero(int val);

void mouse_move_callback(int x, int y);

void mouse_button_callback(int button, int state, int x, int y);

void mouse_wheel_callback(int button, int dir, int x, int y);


#endif
