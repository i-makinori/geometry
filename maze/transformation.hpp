
#ifndef _TRANSFORMATION_H_
#define _TRANSFORMATION_H_

//#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
//#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>


// transformation matrics

// transration
glm::mat4 mat_translation(glm::vec4 trans_xyzw);

// rotate
glm::mat4 mat_rotate_x(double theta_x);
glm::mat4 mat_rotate_y(double theta_y);
glm::mat4 mat_rotate_z(double theta_z);

// Euler rotations
glm::mat4  mat_euler_rotate_xyz(double theta_x, double theta_y, double theta_z);
// Rodrigues rotation
glm::mat4 mat_rodrigues_rotate(glm::vec4 axial_vector, double theta);


#endif
