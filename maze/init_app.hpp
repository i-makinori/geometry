#ifndef INIT_APP_HPP
#define INIT_APP_HPP

#include <GL/glut.h>

#include <GL/freeglut.h>
#include <cmath>
#include <cstdio>
#include <string>

#include <glm/glm.hpp>


#include "camera.hpp"


extern int frame_count;
extern int window_main;

extern Camera camera;


void init();

#endif
