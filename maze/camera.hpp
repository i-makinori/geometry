
#ifndef CAMERA_H_
#define CAMERA_H_

#include <GL/glut.h>
#include <cmath>

#include <glm/glm.hpp>

#include "physics.hpp"
//#define GLM_FORCE_SWIZZLE
//#include "transformation.hpp"

class Camera : public GeometricalPoint {
  // REF: [An OpenGL Flight Simulator]
  // (https://cs.lmu.edu/~ray/notes/flightsimulator/)
  // See also Geometrical Point class
public:
  double theta_fovy;
  //
  float dTheta;
  float velocity; // Absolute Speed
  //
  float dist_to_look;

  using GeometricalPoint::GeometricalPoint;

  Camera();

  void fly(glm::vec4 camera_direction);
  glm::vec4 lookAt();
  float thetaFovy();
  float distToLook ();
  void rePerspect();
  void fovyZoom(int direction);

};
/*
inline void Camera::rePerspect() {
    GLint w = glutGet(GLUT_WINDOW_WIDTH);
    GLint h = glutGet(GLUT_WINDOW_HEIGHT);
    double near = 0.50;
    double far = 150.0;
    //
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(theta_fovy, GLfloat(w) / GLfloat(h), near, far);
    //gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);
    glMatrixMode(GL_MODELVIEW);
}

inline void Camera::fovyZoom(int direction){
  theta_fovy *=
    (  direction < 0 && theta_fovy < 80.0) ? 1*1.05
    : (direction > 0 && theta_fovy > 15.0) ? 1/1.05
    : 1;
  rePerspect();
}
*/

#endif
