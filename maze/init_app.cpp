#ifndef INIT_APP_CPP
#define INIT_APP_CPP

#include "init_app.hpp"


int frame_count = 0;
int window_main;

Camera camera;



void init(){
  GLfloat black[] = {0,0, 0.0, 0.0, 1.0};
  GLfloat yellow[] = {1.0, 1.0, 0.0, 1.0};
  GLfloat cyan[] = {0.0, 1.0, 1.0, 1.0};
  GLfloat white[] = {1.0, 1.0, 1.0, 1.0};
  
  GLfloat direction[] = {1.0, 1.0, 1.0, 0.0};


  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cyan);
  glMaterialfv(GL_FRONT, GL_SPECULAR, white);
  glMaterialf(GL_FRONT, GL_SHININESS, 30);

  glLightfv(GL_LIGHT4, GL_AMBIENT, black);
  glLightfv(GL_LIGHT4, GL_DIFFUSE, yellow);
  glLightfv(GL_LIGHT4, GL_SPECULAR, white);
  glLightfv(GL_LIGHT4, GL_POSITION, direction);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT4);
  glEnable(GL_DEPTH_TEST);
}


#endif
