#ifndef _TRANSFORMATION_CPP_
#define _TRANSFORMATION_CPP_

#include "transformation.hpp"


// math utils

//float v2length(float vx, float vy){
//  return sqrt(pow(vx,2.00)+pow(vy,2.00));
//}



// transformation matrics

// transration

glm::mat4 mat_translation(glm::vec4 trans_xyzw){
  double translate[16] =
    {1, 0, 0, trans_xyzw.x,
     0, 1, 0, trans_xyzw.y,
     0, 0, 1, trans_xyzw.z,
     0, 0, 0, 1};
  return glm::make_mat4(translate);
}

// rotate

glm::mat4 mat_rotate_x(double theta_x){
  double rotate_x[16] =
    {1, 0,             0,            0,
     0, cos(theta_x), -sin(theta_x), 0,
     0, sin(theta_x),  cos(theta_x), 0,
     0, 0,             0,            1};
  return glm::make_mat4(rotate_x);
}

glm::mat4 mat_rotate_y(double theta_y){
  double rotate_x[16] =
    {cos(theta_y),  0, sin(theta_y), 0,
     0,             1, 0,            0,
     -sin(theta_y), 0, cos(theta_y), 0,
     0,             0, 0,            1};
  return glm::make_mat4(rotate_x);
}

glm::mat4 mat_rotate_z(double theta_z){
  double rotate_x[16] =
    {cos(theta_z), -sin(theta_z), 0, 0,
     sin(theta_z),  cos(theta_z), 0, 0,
     0,             0,            0, 0,
     0,             0,            0, 1};
  return glm::make_mat4(rotate_x);
}


// Euler rotations
glm::mat4  mat_euler_rotate_xyz(double theta_x, double theta_y, double theta_z) {
  // product Z (Y X);
  return mat_rotate_z(theta_z) * (mat_rotate_y(theta_y) * (mat_rotate_x(theta_x)));
}

// Rodrigues rotation
glm::mat4 mat_rodrigues_rotate(glm::vec4 axial_vector, double theta){
  // vN :: the Normalized Vector of Axis(Center) for rotation. 
  // vR :: vector to rotate
  // vR' :: vector rotated.
  // vR' = {cos(theta)*E + (1-cos(theta))*vN*trans(vN) + sin(theta)*cross(vN, vN)} * vR
  // ..  = {A + B + C} * vR = ...
  
  glm::vec4 n = glm::normalize(axial_vector); // vector normalized
  double c = cos(theta);
  double h = 1-cos(theta);
  double s = sin(theta);

  double rotate[16] = {
    h*n.x*n.x + c,     h*n.y*n.x - s*n.z, h*n.z*n.x + s*n.y, 0,
    h*n.x*n.y + s*n.z, h*n.y*n.y + c,     h*n.z*n.y - s*n.x, 0,
    h*n.x*n.z - s*n.y, h*n.y*n.z + s*n.x, h*n.z*n.z + c,     0,
    0,                 0,                 0,                 1
  };

  glm::mat4 rot_mat = glm::make_mat4(rotate);
  
  return rot_mat;
}


#endif
