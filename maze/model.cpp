#ifndef _MODEL_C_
#define _MODEL_C_

#include "model.hpp"
#include "init_app.hpp"

extern int mouse_moving;
extern float mouse_x_start;
extern float mouse_y_start;
extern float mouse_x_ago;
extern float mouse_y_ago;
extern float mouse_x_delta;
extern float mouse_y_delta;

void draw_mouse_move_status();
void draw_status_texts(Camera camera, int frame_count);


// fractal terrain
const int num_exp=2;
const int ground_width = pow(2, num_exp) +1;
//float ground_points[ground_width];
float ground_points[ground_width][ground_width];
int calculated_indexes[ground_width][ground_width];

//float height_seed[5] = {+0.00, +0.20, -0.10, -0.15, +0.00};
#include <iostream>

void step_diamond(float chaos_points[ground_width][ground_width], int x, int z, int radias,
                  float default_value=0.00);
void step_square(float chaos_points[ground_width][ground_width], int x, int z, int radias,
                 float default_value=0.00);

int count = 0;

void step_diamond(float chaos_points[ground_width][ground_width], int x, int z, int radias,
                  float default_value){
  if(x >= 0 && x < ground_width && z >= 0 && z < ground_width
     && calculated_indexes[x][z] == 0 ){
    int v1p = (x+radias < ground_width && z+radias < ground_width) ? 1 : 0;
    int v2p = (x-radias >= 0           && z+radias < ground_width) ? 1 : 0;
    int v3p = (z+radias < ground_width && z-radias >= 0) ? 1 : 0;
    int v4p = (z-radias >= 0           && z-radias >= 0) ? 1 : 0;
    float v1 = v1p ? chaos_points[x+radias][z+radias] : 0;
    float v2 = v2p ? chaos_points[x-radias][z+radias] : 0;
    float v3 = v3p ? chaos_points[x+radias][z-radias] : 0;
    float v4 = v4p ? chaos_points[x-radias][z-radias] : 0;
    //
    float average = (v1+v2+v3+v4)/(v1p+v2p+v3p+v4p);
    //float chaos = (-0.5+(float)rand()/RAND_MAX)*radias; ///ground_width*4.00*1/3;
    float chaos = (-0.5+(float)rand()/RAND_MAX)*radias/ground_width*1.00;
    float center_value = average + chaos;
    if(default_value==0.00){
      chaos_points[x][z] = center_value;
    } else {
      chaos_points[x][z] = default_value;
    }
    calculated_indexes[x][z] = 1;
    //
    count++;
    std::cout << "Count: " << count << ", D: " << x << ", " << z
              << ", R:" << radias << ", Value: " << center_value << "\n";
    //
    int dcenter = radias;
    if(dcenter>=1){
      step_square(chaos_points, x+dcenter, z, dcenter);
      step_square(chaos_points, x-dcenter, z, dcenter);
      step_square(chaos_points, x, z+dcenter, dcenter);
      step_square(chaos_points, x, z-dcenter, dcenter);
    }
  }
}

void step_square(float chaos_points[ground_width][ground_width], int x, int z, int radias,
                 float default_value){
  if((x >= 0 && x < ground_width && z >= 0 && z < ground_width)
     && calculated_indexes[x][z] == 0 ){
    int v1p = (x+radias < ground_width) ? 1 : 0;
    int v2p = (x-radias >= 0)           ? 1 : 0;
    int v3p = (z+radias < ground_width) ? 1 : 0;
    int v4p = (z+radias >= 0)           ? 1 : 0;
    float v1 = v1p ? chaos_points[x+radias][z] : 0;
    float v2 = v2p ? chaos_points[x-radias][z] : 0;
    float v3 = v3p ? chaos_points[x][z+radias] : 0;
    float v4 = v4p ? chaos_points[x][z-radias] : 0;
    //
    float average = (v1+v2+v3+v4)/(v1p+v2p+v3p+v4p);
    float chaos = (-0.5+(float)rand()/RAND_MAX)*radias/ground_width*1.00;
    float center_value = average + chaos;
    if (default_value==0.00){
      chaos_points[x][z] = default_value;
    } else {
      chaos_points[x][z] = center_value;
    }
    calculated_indexes[x][z] = 1;
    //
    count++;
    std::cout << "Count: " << count << ", S: " << x << ", " << z
              << ", R:" << radias << ", Value: " << center_value << "\n";
    //
    int dcenter = roundf(radias/2);  
    if(dcenter>=1){
      step_diamond(chaos_points, x+dcenter, z+dcenter, dcenter);
      step_diamond(chaos_points, x-dcenter, z+dcenter, dcenter);
      step_diamond(chaos_points, x+dcenter, z-dcenter, dcenter);
      step_diamond(chaos_points, x-dcenter, z-dcenter, dcenter);
    }
  }
}

void init_model(){
  int radias = round((ground_width-1)/2);
  std::cout << "R: " << radias << "\n";
  ground_points[0][0] = 0;
  ground_points[0][ground_width-1] = 0;
  // Diamond Square Map
  step_diamond(ground_points, radias, radias, radias, 2.00);
  //std::cout << "map generated\n";
}


void draw_ground_points(float chaos_points[ground_width][ground_width]){
  float d_geo=0.2;

  // Lines
  for(int x=0; x<ground_width-1; x++){
    for(int z=0; z<ground_width; z++){
      draw_line(glm::vec4(d_geo*x, chaos_points[x][z],                      d_geo*z, 0),
                glm::vec4(d_geo,   chaos_points[x+1][z]-chaos_points[x][z], 0,       0));
    }
  }
  for(int z=0; z<ground_width-1; z++){
    for(int x=0; x<ground_width; x++){
      draw_line(glm::vec4(d_geo*x, chaos_points[x][z],                      d_geo*z, 0),
                glm::vec4(0,       chaos_points[x][z+1]-chaos_points[x][z], d_geo,   0));
    }
  }
  
  
  // Triangles
  /*
  glBegin(GL_TRIANGLES);{
    for (int x=0; x<ground_width; x++){
      for(int z=0; z<ground_width; z++){
        // pa-pb-pc
        glVertex3f(d_geo*x,     chaos_points[x][z],   d_geo*z);
        glVertex3f(d_geo*(x+1), chaos_points[x+1][z], d_geo*z);
        glVertex3f(d_geo*x,     chaos_points[x][z+1], d_geo*(z+1));
        // pa-pd-pc
        glVertex3f(d_geo*(x+1), chaos_points[x+1][z],   d_geo*z);
        glVertex3f(d_geo*(x+1), chaos_points[x+1][z+1], d_geo*(z+1));
        glVertex3f(d_geo*x,     chaos_points[x][z+1],   d_geo*(z+1));
      }
    }
  } glEnd();
  */
}


void draw(){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();{
    
    // Rotate the scene so we can see the top of spheres.
    // glRotatef(-20.0, 1.0, 0.0, 0.0);
    // draw_tube(0.5, 1);

    glPushMatrix();
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_cyan);
    glTranslatef(-0.75, 0.5, 0.0);
    glRotatef(90.0, 1.0, 0.0, 0.0);
    draw_tube(0.2, 0.5);
    glPopMatrix();

    //
    draw_axis();
    draw_real_arrow(glm::vec4(1,1,3,0), glm::vec4(1, 3, 2, 0));
    draw_real_arrow(glm::vec4(-2,0.5,1.5,0), glm::vec4(0,0,1,0));

    // draw ground;
    // Draw Diamond Square Map
    draw_ground_points(ground_points);
    
    /*
    //cyan
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_cyan);
    
    // plane
    glPushMatrix();
    glTranslatef(0.60, -0.8, -1.0);
    glBegin(GL_QUADS); {
      glVertex3d(-1.5, -0.5, 0);
      glVertex3d(-1.5,  0.5, 0);
      glVertex3d(0.5,   0.5, 10);
      glVertex3d(0.5,  -0.5, 10);
    } glEnd();
  
    glPopMatrix();
    
    
    // Torus
    glPushMatrix();
    glTranslatef(-0.75, 0.5, 0.0);
    glRotatef(90.0, 1.0, 0.0, 0.0);
    glutWireTorus(0.275, 0.85, 16, 40); // or Solid

    glPopMatrix();

    // Cone
    glPushMatrix();
    glTranslatef(-0.75, -0.5, 0.0);
    glRotatef(270.0, 1.0, 0.0, 0.0);
    glutWireCone(1.0, 2.0, 70, 12); // or Solid
    glPopMatrix();

    // Add sphere to the scene
    glPushMatrix();
    glTranslatef(0.75, 0.0, -1.0);
    glutWireSphere(1.0, 30, 30);
    glPopMatrix();

    // sphere
    glPushMatrix();
    glTranslatef(0.60, -0.8, -1.0);
    glRotatef(frame_count*0.5, frame_count*1.0, frame_count*0.2, frame_count*1.0);
    glRotatef(0.5, 1.0, 0.2, 1.0);
    glutWireSphere(1.0, 20, 30);
    glPopMatrix();
    */
    // draw status texts
    draw_mouse_move_status();
    draw_status_texts(camera, frame_count);
  
    //
  } glPopMatrix();
  glFlush();
}

void display() {
  glClearColor(0.1f, 0.15, 0.15, 0.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  
  camera.rePerspect();
  glm::vec4 eye = (camera.getPosition());
  glm::vec4 at = camera.lookAt();
  glm::vec4 up = (camera.getVertical());
  gluLookAt(eye.x, eye.y, eye.z,
            at.x, at.y, at.z,
            up.x, up.y, up.z);

  draw();
  
  //glFlush();
  glFinish();
  glutSwapBuffers();
}


// position.
void draw_mouse_move_status(){
  if (mouse_moving){
    DrawString("pressing", 10, 10, 1, 0.8);
    std::string str0 =
      "xstart: " + std::to_string(mouse_x_start)
      + ", ystart: " + std::to_string(mouse_y_start);
    DrawString(str0, 10, 10, 1, 1.0);
    std::string str1 =
      "xago: " + std::to_string(mouse_x_ago)
      + ", yago: " + std::to_string(mouse_y_ago)
      + ", dx: " + std::to_string(mouse_x_delta)
      + ", dy: " + std::to_string(mouse_y_delta);
    DrawString(str1, 10, 10, 1, 1.2);
    GLint w = glutGet(GLUT_WINDOW_WIDTH);
    GLint h = glutGet(GLUT_WINDOW_HEIGHT);
    std::string str2 =
      "wid: " + std::to_string(w)
      + ", hei: " + std::to_string(h);
    DrawString(str2, 10, 10, 1, 1.4);
      
  } else if (!mouse_moving){
    DrawString("rereasing", 10, 10, 1, 1);
  }
}

void draw_status_texts(Camera camera, int frame_count){
  GLint w = glutGet(GLUT_WINDOW_WIDTH);
  GLint h = glutGet(GLUT_WINDOW_HEIGHT);

  
  glm::vec4 camera_dir = camera.getDirection();
  std::string text;
  text =
    "dirx:" + std::to_string(  camera_dir.x) +
    ", diry:" + std::to_string(camera_dir.y) +
    ", dirz:" + std::to_string(camera_dir.z);
  DrawString(text, w, h ,100,400);

  glm::vec4 at = (camera.lookAt());
  text =
    "atx:" + std::to_string(  at.x) +
    ", aty:" + std::to_string(at.y) +
    ", atz:" + std::to_string(at.z);
  DrawString(text, w, h ,100,500);
  
  glm::vec4 camera_xyz = camera.getPosition();
  text =
    "posx:"   + std::to_string(camera_xyz.x) +
    ", posy:" + std::to_string(camera_xyz.y) +
    ", posz:" + std::to_string(camera_xyz.z);
  DrawString(text, w, h ,100,525);
  
  text =
    "count:" + std::to_string(frame_count);
  DrawString(text, w, h ,100,550);
}


#endif
