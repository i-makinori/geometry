
#ifndef CAMERA_CPP_
#define CAMERA_CPP_

#include "camera.hpp"

//class Camera : public GeometricalPoint {
  // REF: [An OpenGL Flight Simulator]
  // (https://cs.lmu.edu/~ray/notes/flightsimulator/)
  // See also Geometrical Point class
//public:
//  double theta_fovy;
  //
//  float dTheta;
//  float velocity; // Absolute Speed
  //
//  float dist_to_look;

//  using GeometricalPoint::GeometricalPoint;

Camera::Camera() : GeometricalPoint() {
    position = glm::vec4(-3, 3, -10, 0);
  
    velocity = 1.00 / 20.00;
    dTheta = 0.02;
    theta_fovy = 40.0;
    dist_to_look = 50.0;
};

void Camera::fly(glm::vec4 camera_direction) {
    // glm::vec4(right, up, forward, 0)
    position = position + velocity * glm::normalize(camera_direction);
}

glm::vec4 Camera::lookAt() {
    return getPosition() + getDirection()*distToLook();
  }
float Camera::thetaFovy() { return theta_fovy;}
float Camera::distToLook () { return dist_to_look; }
//void Camera::rePerspect();
//void Camera::fovyZoom(int direction);



void Camera::rePerspect() {
    GLint w = glutGet(GLUT_WINDOW_WIDTH);
    GLint h = glutGet(GLUT_WINDOW_HEIGHT);
    double near = 0.50;
    double far = 150.0;
    //
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(theta_fovy, GLfloat(w) / GLfloat(h), near, far);
    //gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);
    glMatrixMode(GL_MODELVIEW);
}

void Camera::fovyZoom(int direction){
  theta_fovy *=
    (  direction < 0 && theta_fovy < 80.0) ? 1*1.05
    : (direction > 0 && theta_fovy > 15.0) ? 1/1.05
    : 1;
  rePerspect();
}


#endif
