
#ifndef _PHYSICS_CPP_
#define _PHYSICS_CPP_

#include "physics.hpp"

// 1. geometry

glm::vec3 v123(glm::vec4 vec4){
  return glm::vec3(vec4.x, vec4.y, vec4.z);
}

glm::vec4 cross123_of_vec4(glm::vec4 v1, glm::vec4 v2, float fourth = 0.0){
  return glm::vec4(glm::cross(v123(v1), v123(v2)),
                   fourth);
  
}

GeometricalPoint::GeometricalPoint(){
    position = glm::vec4(0, 0, 0, 0);
    forward = glm::vec4(0, 0, 1, 0);
    up = glm::vec4(0, 1, 0, 0);
    // ?- which direction system is better Right hand or Left hand ?
    right = cross123_of_vec4(forward, up, 0);
    //right = glm::vec4(1, 0, 0, 0);
  }

glm::vec4 GeometricalPoint::forwardVec(){ return forward;}
glm::vec4 GeometricalPoint::upVec(){ return up;}
glm::vec4 GeometricalPoint::rightVec(){ return right;}

glm::vec4 GeometricalPoint::getPosition() { return position; }
glm::vec4 GeometricalPoint::getDirection() { return forward; }
glm::vec4 GeometricalPoint::getVertical() { return up; }

void GeometricalPoint::pitch(double angle) {
  forward = glm::normalize(forward * (float)cos(angle) + up * (float)sin(angle));
  up = cross123_of_vec4(right, forward, 0);
}
void GeometricalPoint::roll(double angle) {
  right = glm::normalize(right * (float)cos(angle) + up * (float)sin(angle));
  up = cross123_of_vec4(right, forward, 0);
}
void GeometricalPoint::yaw(double angle){
  right = glm::normalize(right * (float)cos(angle) + forward * (float)sin(angle));
  forward = cross123_of_vec4(up, right, 0);
}
  

// 1. collision_detection

#define boolean int
boolean collision_detection() {
  //return True;
  return 0;
};

// 1. physical_substance

glm::vec4 gravity = glm::vec4(0, 0, 9.8, 1.00); // 9.8[m/s^2] to Z-axis




// class mass_point
  
 

// class virtual_physical_substance


#endif
