#ifndef HANDLE_INPUT_CPP
#define HANDLE_INPUT_CPP

#include "handle_input.hpp"

//

//extern Camera camera;

int mouse_moving;
float mouse_x_start;
float mouse_y_start;
float mouse_x_ago;
float mouse_y_ago;
float mouse_x_delta;
float mouse_y_delta;

// exit
int exit_simulator() {
  glutDestroyWindow (window_main);
  exit(0);
  return 0;
}

// On reshape, constructs a camera that perfectly fits the window.
void reshape(GLint w, GLint h) {
  glViewport(10, 10, w-20, h-20); // 10px border
  camera.rePerspect();
}

// Moves the camera according to the key pressed, then ask to refresh the
// display.
void specialkey_callback(int key, int, int) {
  switch (key) {
    case GLUT_KEY_LEFT: camera.yaw(0.02); break;
    case GLUT_KEY_RIGHT: camera.yaw(-0.02); break;
    case GLUT_KEY_UP: camera.pitch(0.02); break;
    case GLUT_KEY_DOWN: camera.pitch(-0.02); break;
    case GLUT_KEY_PAGE_UP: camera.roll(-0.02); break;
    case GLUT_KEY_PAGE_DOWN: camera.roll(0.02); break;
  }
  glutPostRedisplay();
}


void keyboard_callback(unsigned char key, int x, int y){

  {    } if(key=='w'){ // forward
    camera.fly(camera.forwardVec());
  } else if(key=='s'){ // back
    camera.fly(-camera.forwardVec());
  } else if(key=='a'){ // left
    camera.fly(-camera.rightVec());
  } else if(key=='d'){ //right
    camera.fly(camera.rightVec());
  } else if(key=='e'){ // Ascend
    camera.fly(camera.upVec());
  } else if (key=='z') { // Descend
    camera.fly(-camera.upVec());
  } else if (key=='\e') { // if Escape
    exit_simulator();
  }
  glutPostRedisplay();
}


int truncate_almost_zero(int val){
  return (abs(val)<=1)? 0 : val;
}

void mouse_move_callback(int x, int y) {
  if(mouse_moving){
    //mouse_x_delta = truncate_almost_zero(x - mouse_x_ago);
    //mouse_y_delta = truncate_almost_zero(y - mouse_y_ago);
    mouse_x_delta = x - mouse_x_ago;
    mouse_y_delta = y - mouse_y_ago;

    GLint w = glutGet(GLUT_WINDOW_WIDTH);
    GLint h = glutGet(GLUT_WINDOW_HEIGHT);

    float dtheta_x =
      - (double)mouse_x_delta/w * glm::radians(camera.thetaFovy()*(double)w/h)/M_2_PI;
    camera.yaw(dtheta_x);

    float dtheta_y =
      - (double)mouse_y_delta/h * glm::radians(camera.thetaFovy()*(double)h/h)/M_2_PI;
    camera.pitch(dtheta_y);
  }
  mouse_x_ago = x;
  mouse_y_ago = y;
}

void mouse_button_callback(int button, int state, int x, int y){
  // GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, or GLUT_RIGHT_BUTTON
  if (state==GLUT_DOWN) {
    mouse_x_start = x;
    mouse_y_start = y;
    mouse_x_ago = x;
    mouse_y_ago = y;
    mouse_moving = true;
  } else if (state==GLUT_UP) {
    mouse_moving = false;
  }
}

void mouse_wheel_callback(int button, int dir, int x, int y){
  if (dir > 0) {
    camera.fovyZoom(+1.00);
  } else {
    camera.fovyZoom(-1.00);
  }
}


#endif
