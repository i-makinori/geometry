
#ifndef GLUTILS_H_
#define GLUTILS_H_

#include <cstdio>
#include <string>

#include <GL/glut.h>
#include <glm/glm.hpp>
#include <cmath>


// Utils

void push_to_screen_procedures(float virtual_width, float virtual_height);


extern void pop_from_screen_procedures();

// Draw Text

extern void DrawString(std::string str, float virtual_width, float virtual_height, float x0, float y0);


// Color

extern GLfloat color_white[4];
extern GLfloat color_red[4];
extern GLfloat color_green[4];
extern GLfloat color_blue[4];
extern GLfloat color_cyan[4];


// necessary Models

void draw_line(glm::vec4 vec_start, glm::vec4 vec_delta, GLfloat color[] = color_white);
void draw_tube(float radius, float height, int sides = 12);
void draw_axis();
void draw_real_arrow(glm::vec4 vec_from, glm::vec4 vec_delta);



#endif
