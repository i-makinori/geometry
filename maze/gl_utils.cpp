
#ifndef GLUTILS_CPP_
#define GLUTILS_CPP_

#include "gl_utils.hpp"


// Utils

void push_to_screen_procedures(float virtual_width, float virtual_height) {
  glDisable(GL_LIGHTING);
  // parallel projection
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0, virtual_width, virtual_height, 0);
  //
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
}

extern void pop_from_screen_procedures(){
  // collection
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  //
  glEnable(GL_LIGHTING);
}


// Draw Text

extern void DrawString(std::string str, float virtual_width, float virtual_height, float x0, float y0)
{

  push_to_screen_procedures(virtual_width, virtual_height);
  // draw text to screen
  glRasterPos2f(x0, y0);
  //int size = strlen(str);
  int size=str.length();
  for(int i = 0; i < size; i++){   
    //glColor3d(1,1,1);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, str[i]);
    //glLogicOp(GL_SET);
  }
  pop_from_screen_procedures();
}


// Color

GLfloat color_white[4] = {1.0, 1.0, 1.0, 1.0};
GLfloat color_red[4]   = {1.0, 0.0, 0.0, 1.0};
GLfloat color_green[4] = {0.0, 1.0, 0.0, 1.0};
GLfloat color_blue[4]  = {0.0, 0.0, 1.0, 1.0};
GLfloat color_cyan[4]  = {0.0, 1.0, 1.0, 1.0};


// necessary Models

void draw_line(glm::vec4 vec_start, glm::vec4 vec_delta, GLfloat color[]){
  glm::vec4 vec_to_at = vec_start + vec_delta;
  // line
  glPushMatrix();

  glBegin(GL_LINES); {
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
    glVertex3f(vec_start[0],vec_start[1],vec_start[2]);
    glVertex3f(vec_to_at[0],vec_to_at[1],vec_to_at[2]);
  } glEnd();
  
  glPopMatrix();
}

void draw_tube(float radius, float height, int sides){
  int i=0;
  double step = 2*M_PI/double(sides);
  /* upper side */
  glNormal3d(0.0, 1.0, 0.0);
  glBegin(GL_POLYGON);
  for(i = 0; i < sides; i++) {
    double t = step * (double)i;
    glVertex3d(radius * sin(t), height, radius * cos(t));
  }
  glEnd();
  /* bottom side */
  glNormal3d(0.0, -1.0, 0.0);
  glBegin(GL_POLYGON);
  for(i = sides; --i >= 0; ) {
    double t = step * (double)i;
    glVertex3d(radius * sin(t), 0.0, radius * cos(t));
  }
  glEnd();
  /* flanc side */
  glBegin(GL_QUAD_STRIP);
  for (i = 0; i <= sides; i++) {
    double t = step * (double)i;
    double x = sin(t);
    double z = cos(t);

    glNormal3d(x, 0.0, z);
    glVertex3f(radius * x, height, radius * z);
    glVertex3f(radius * x, 0.0, radius * z);
  }
  glEnd();
}

void draw_axis (){
  glm::vec4 origin_point = glm::vec4(0, 0, 0, 0);
  glm::vec4 x_base_vec = glm::vec4(1, 0, 0, 0);
  glm::vec4 y_base_vec = glm::vec4(0, 1, 0, 0);
  glm::vec4 z_base_vec = glm::vec4(0, 0, 1, 0);

  // x axis
  draw_line(origin_point, x_base_vec, color_red);
  glPushMatrix();{
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_red);
    glTranslatef(0.0, 0.0, 0.0);
    glRotatef(-90.0, 0.0, 0.0, 1.0); 
    draw_tube(0.01f, 0.8f);
  } glPopMatrix();
  glPushMatrix();{
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_red);
    glTranslatef(0.8, 0.0, 0.0);
    glRotatef(90.0, 0.0, 1.0, 0.0);
    glutWireCone(0.2, 0.2, 6, 2);
  } glPopMatrix();
  // y axis
  draw_line(origin_point, y_base_vec, color_green);
  glPushMatrix();{
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_green);
    glTranslatef(0.0, 0.0, 0.0);
    glRotatef(0.0, 0.0, 1.0, 0.0); 
    draw_tube(0.01, 0.8);
  } glPopMatrix();
    glPushMatrix();{
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_green);
    glTranslatef(0.0, 0.8, 0.0);
    glRotatef(-90.0, 1.0, 0.0, 0.0);
    glutWireCone(0.2, 0.2, 6, 2);
  } glPopMatrix();
  // z axis
  draw_line(origin_point, z_base_vec, color_blue);  
  glPushMatrix();{
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_blue);
    glTranslatef(0.0, 0.0, 0.0);
    glRotatef(90.0, 1.0, 0.0, 0.0);
    draw_tube(0.01, 0.8);
  } glPopMatrix();
  glPushMatrix();{
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color_blue);
    glTranslatef(0.0, 0.0, 0.8);
    glRotatef(0.0, 0.0, 0.0, 1.0);
    glutWireCone(0.2, 0.2, 6, 2);
  } glPopMatrix();
}


void draw_real_arrow(glm::vec4 vec_from, glm::vec4 vec_delta){
  float len = glm::length(vec_delta);
  if(len>0.0){
    float degree = glm::degrees(acos(vec_delta.z/len));
    glm::vec4 vec_to_at = vec_from + vec_delta;
  
    //line
    draw_line(vec_from, vec_delta);
    glPushMatrix();{
      glTranslatef(vec_from.x, vec_from.y, vec_from.z);
      glRotatef(degree, -vec_delta.y, vec_delta.x, 0);
      
      //glPushMatrix();{
      //draw_axis();
      //} glPopMatrix();

      // tube
      glPushMatrix();{
        glRotatef(90.0, 1.0, 0.0, 0.0);
        draw_tube(0.015, glm::length(vec_delta)-0.10);
      } glPopMatrix();
    
      // Cone
      glPushMatrix();{
        glTranslatef(0, 0, len-0.10);
        glRotatef(0.0, 0,0,0);
        glutSolidCone(0.05, 0.10, 6, 1);
      } glPopMatrix();
    } glPopMatrix();
  }
}

#endif
