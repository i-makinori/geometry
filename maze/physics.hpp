
#ifndef _PHYSICS_H_
#define _PHYSICS_H_

#include <glm/glm.hpp>

// 1. geometry

glm::vec3 v123(glm::vec4 vec4);

//glm::vec4 cross123_of_vec4(glm::vec4 v1, glm::vec4 v2, float fourth = 0.0);
glm::vec4 cross123_of_vec4(glm::vec4 v1, glm::vec4 v2, float fourth);

class GeometricalPoint {
  // REF: [An OpenGL Flight Simulator]
  // (https://cs.lmu.edu/~ray/notes/flightsimulator/)
  // See also Camera class
  //
  
public:
  glm::vec4 position; // coordinate in the world t(x y z w)
  glm::vec4 forward, up, right; // direction of point


  GeometricalPoint();
  
  //
  glm::vec4 forwardVec();
  glm::vec4 upVec();
  glm::vec4 rightVec();

  glm::vec4 getPosition();
  glm::vec4 getDirection();
  glm::vec4 getVertical();
  
  //
  void pitch(double angle);
  void roll(double angle);
  void yaw(double angle);
  
};

// 1. collision_detection

#define boolean int
boolean collision_detection();

// 1. physical_substance

class mass_point {
};
 

class virtual_physical_substance {
};


#endif
