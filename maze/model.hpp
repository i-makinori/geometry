#ifndef _MODEL_H_
#define _MODEL_H_


#include <GL/glut.h>

#include <GL/freeglut.h>
#include <cmath>
#include <cstdio>
#include <string>

#include <glm/glm.hpp>


#include "camera.hpp"
#include "gl_utils.hpp"
#include "transformation.hpp"

void init_model();
void draw();

void display();

#endif
