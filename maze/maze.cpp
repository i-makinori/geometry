#ifndef _maze_C_
#define _maze_C_

#include <GL/glut.h>

#include <GL/freeglut.h>
#include <cmath>
#include <cstdio>
#include <string>

#include <glm/glm.hpp>

#include "camera.hpp"
#include "gl_utils.hpp"
#include "handle_input.hpp"
#include "transformation.hpp"
#include "model.hpp"

// 

class Maze{
  
};


// Requests to draw the next frame.

void timer(int v) {
  glutPostRedisplay();
  glutTimerFunc(1000/60, timer, v);
  frame_count += 1;
}

// main procedure
int main(int argc, char** argv){
  //
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowPosition(80, 80);
  glutInitWindowSize(800, 600);
  glutCreateWindow("Cyan Spheres in Yellow Light");
  //
  glutReshapeFunc(reshape);
  glutDisplayFunc(display);
  glutTimerFunc(100, timer, 0);
  //
  glutSpecialFunc(specialkey_callback);
  glutKeyboardFunc(keyboard_callback);
  glutMotionFunc(mouse_move_callback); // mouse move if pressed
  glutPassiveMotionFunc(mouse_move_callback); // mouse move if not pressed
  glutMouseFunc(mouse_button_callback);
  glutMouseWheelFunc(mouse_wheel_callback);

  //
  init();
  init_model();
  glutMainLoop();
}

  
#endif

