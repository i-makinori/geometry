
#ifndef CAMERA_H_
#define CAMERA_H_

#include <GL/glut.h>
#include <cmath>

#include <glm/glm.hpp>
//#define GLM_FORCE_SWIZZLE
#include "transformation.hpp"

glm::vec3 v123(glm::vec4 vec4){
  return glm::vec3(vec4.x, vec4.y, vec4.z);
}

class Camera {
  // REF: [An OpenGL Flight Simulator]
  // (https://cs.lmu.edu/~ray/notes/flightsimulator/)
  glm::vec4 position; // position in the world t(x y z w)
  glm::vec4 forward, up, right;
  //
  glm::vec4 look_at;
  //
  double theta_fovy;
  //
  float dTheta;
  float velocity; // Absolute Speed
  //
  float dist_to_look;
public:
  Camera(){
    position = glm::vec4(10, 3, 10, 0);
    forward = glm::vec4(0, 0, -1, 0);
    up = glm::vec4(0, 1, 0, 0);
    right = glm::vec4(1, 0, 0, 0);
    velocity = 1.00 / 20.00;
    dTheta = 0.02;
    theta_fovy = 40.0;
    dist_to_look = 50.0;
  }
  void fly(glm::vec4 camera_direction) {
    // glm::vec4(right, up, forward, 0)
    position = position + velocity * glm::normalize(camera_direction);
  }

  glm::vec4 forwardVec(){ return forward;}
  glm::vec4 upVec(){ return up;}
  glm::vec4 rightVec(){ return right;}
  
  glm::vec4 getPosition() { return position; }
  glm::vec4 getDirection() { return forward; }
  glm::vec4 lookAt() {
    return getPosition() + getDirection()*distToLook();
  }
  glm::vec4 getVertical() { return up; }
  float thetaFovy() { return theta_fovy;}

  float distToLook () { return dist_to_look; }
  
  void pitch(double angle) {
    forward = glm::normalize(forward * (float)cos(angle) + up * (float)sin(angle));
    up = glm::vec4(glm::cross(v123(right), v123(forward))
                   , 0);
  }
  void roll(double angle) {
    right = glm::normalize(right * (float)cos(angle) + up * (float)sin(angle));
    up = glm::vec4(glm::cross(v123(right), v123(forward))
                   , 0);
  }
  void yaw(double angle){
    right = glm::normalize(right * (float)cos(angle) + forward * (float)sin(angle));
    forward = glm::vec4(glm::cross(v123(up), v123(right))
                        , 0);
  }
  void rePerspect();
  void fovyZoom(int direction);
  
};

inline void Camera::rePerspect() {
    GLint w = glutGet(GLUT_WINDOW_WIDTH);
    GLint h = glutGet(GLUT_WINDOW_HEIGHT);
    double near = 0.50;
    double far = 150.0;
    //
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(theta_fovy, GLfloat(w) / GLfloat(h), near, far);
    //gluPerspective(40.0, GLfloat(w) / GLfloat(h), 1.0, 150.0);
    glMatrixMode(GL_MODELVIEW);
}

inline void Camera::fovyZoom(int direction){
  theta_fovy *=
    (  direction < 0 && theta_fovy < 80.0) ? 1*1.05
    : (direction > 0 && theta_fovy > 15.0) ? 1/1.05
    : 1;
  rePerspect();
}

/*
 void reRotate(){
    // LT1 (LookingTo1) : horizonal direction of looking before vertical rotation.
    // nA: Axel of vertical rotation for far point. (nA ⊥ CV1) ∧ (nA  ∈ XZ_plane)
    // and, rotate vertically
    glm::vec4 LT1 = mat_rotate_y(thetaX) * glm::vec4(1, 0, 0, 0); 
    glm::vec4 nA1 = mat_rotate_y(thetaX) * glm::vec4(0, 0, 1, 0);
    look_to = mat_rodrigues_rotate(nA1, -thetaY) * (LT1*float(dist()));
    
    // roll
    //up_vec = glm::vec4(cos(thetaR), -sin(thetaR), 0, 0.00);
    //up_vec = glm::vec4(0,1,0,0
*/
/*
  void reRotate(){
  // (1,0,0,0) :--(horizonal rotation(thetaH))-->> CV1
  // CV1 :--(vertical rotation(nA, thetaY))-->> eye_pos
  //
  // CV1 (CameraVector1) : horizonal direction of camera before vertical rotation.
  glm::vec4 CV1 = mat_rotate_y(thetaH) * glm::vec4(1, 0, 0, 0); // * float(dist());
  // Axel of vertical rotation. (nA ⊥ CV1) ∧ (nA  ∈ XZ_plane)
  glm::vec4 nA = mat_rotate_y(thetaH) * glm::vec4(0, 0, 1, 0);
  // rotate vertically
  eye_pos = mat_rodrigues_rotate(nA, thetaY) * (CV1*float(dist()));
  //
  // roll
  //up_vec = glm::vec4(cos(thetaR), -sin(thetaR), 0, 0.00);
  up_vec = glm::vec4(0,1,0,0);
  }
*/

#endif
