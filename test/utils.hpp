

#ifndef UTILS_H_
#define UTILS_H_

#include <GL/glut.h>
#include <cmath>

#include "utils.hpp"


// Utils

void push_to_screen_procedures(float virtual_width, float virtual_height) {
  glDisable(GL_LIGHTING);
  // parallel projection
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0, virtual_width, virtual_height, 0);
  //
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
}

extern void pop_from_screen_procedures(){
  // collection
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  //
  glEnable(GL_LIGHTING);
}


// Draw Text

#include <cstdio>
#include <string>


extern void DrawString(std::string str, float virtual_width, float virtual_height, float x0, float y0)
{

  push_to_screen_procedures(virtual_width, virtual_height);
  // draw text to screen
  glRasterPos2f(x0, y0);
  //int size = strlen(str);
  int size=str.length();
  for(int i = 0; i < size; i++){   
    //glColor3d(1,1,1);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, str[i]);
    //glLogicOp(GL_SET);
  }
  pop_from_screen_procedures();
}

#endif
