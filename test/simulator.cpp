#ifndef _simulator_C_
#define _simulator_C_

//
// This application shows balls bouncing on a checkerboard, with no respect
// for the laws of Newtonian Mechanics.  There's a little spotlight to make
// the animation interesting, and arrow keys move the camera for even more
// fun.

//#include <GL/glut.h>
#include <GL/freeglut.h>
#include <cmath>
#include <cstdio>
#include <string>
//
#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>
//
#include "utils.hpp"
#include "camera.hpp"
#include "models.hpp"


// Global variables: a camera, a checkerboard and some balls.
Checkerboard checkerboard(8, 8);
Camera camera;
Ball balls[] = {
  Ball(1.0, GREEN, 7, 6, 1),
  Ball(1.5, MAGENTA, 6, 3, 4),
  Ball(0.4, WHITE, 5, 1, 7),
  Ball(0.2, WHITE, 10, 2, 8),
};

int frame_count = 0;
int window_main;

int mouse_moving;
float mouse_x_start;
float mouse_y_start;
float mouse_x_ago;
float mouse_y_ago;
float mouse_x_delta;
float mouse_y_delta;

// Application-specific initialization: Set up global lighting parameters
// and create display lists.
void init() {
  glEnable(GL_DEPTH_TEST);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, WHITE);
  glLightfv(GL_LIGHT0, GL_SPECULAR, WHITE);
  glMaterialfv(GL_FRONT, GL_SPECULAR, WHITE);
  glMaterialf(GL_FRONT, GL_SHININESS, 30);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  checkerboard.create();
}

// Draws one frame, the checkerboard then the balls, from the current camera
// position.
void draw_mouse_move_status(){
  if (mouse_moving){
    DrawString("pressing", 10, 10, 1, 0.8);
    std::string str0 =
      "xstart: " + std::to_string(mouse_x_start)
      + ", ystart: " + std::to_string(mouse_y_start);
    DrawString(str0, 10, 10, 1, 1.0);
    std::string str1 =
      "xago: " + std::to_string(mouse_x_ago)
      + ", yago: " + std::to_string(mouse_y_ago)
      + ", dx: " + std::to_string(mouse_x_delta)
      + ", dy: " + std::to_string(mouse_y_delta);
    DrawString(str1, 10, 10, 1, 1.2);
    GLint w = glutGet(GLUT_WINDOW_WIDTH);
    GLint h = glutGet(GLUT_WINDOW_HEIGHT);
    std::string str2 =
      "wid: " + std::to_string(w)
      + ", hei: " + std::to_string(h);
    DrawString(str2, 10, 10, 1, 1.4);
      
  } else if (!mouse_moving){
    DrawString("rereasing", 10, 10, 1, 1);
  }
}

void draw_look_point (Camera camera) {

  GLfloat color[] = {0.5, 1, 1};

  glm::vec4 look_at = camera.getPosition() + camera.forwardVec()*(float)5.00;
  glm::mat4 trans_to = mat_translation(look_at);

  //glutSolidSphere(5, 30, 30);
  /*
  std::string str0 =
    "at_x: " + std::to_string(look_at.x)
    + ", at_y: " + std::to_string(look_at.y)
    + ", at_z: " + std::to_string(look_at.z);
  glColor3fv(color);
  DrawString(str0, 10, 10, 1, 2.0);
  */
  
  glPushMatrix();
  glDisable(GL_LIGHTING);
  glTranslated(look_at.x, look_at.y, look_at.z);
  glBegin(GL_LINE_LOOP);

  glm::vec4 circle_point;
  int segs = 32;
  float theta;
  
  for (int i = 0; i < segs; i++) {
    theta = 2*M_PI*i/segs;
    circle_point =
      //trans_to *
      mat_rodrigues_rotate(camera.forwardVec(), theta)
      * camera.rightVec() * (float)1.00;
    glColor3fv(color);
    glVertex3f(circle_point.x, circle_point.y, circle_point.z);
  }

  glEnd();
  glEnable(GL_LIGHTING);
  glPopMatrix();
}

void draw(){
  glMatrixMode(GL_MODELVIEW); {
    checkerboard.draw();
    for (int i = 0; i < sizeof(balls) / sizeof(Ball); i++){
      balls[i].update();
    }
    draw_axes(20.0);
    draw_look_point(camera);
  }
  glMatrixMode(GL_PROJECTION); {
    glColor3f(1, 1, 0.5);
    draw_2d_rect(10,10,50,100);
    glColor3f(1, 1, 1);
    draw_status_texts(camera, frame_count);
    draw_mouse_move_status();
  }
  glLoadIdentity();
}

void display() {
    
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  
  camera.rePerspect();
  
  glm::vec4 eye = (camera.getPosition());
  glm::vec4 at = camera.lookAt();
  glm::vec4 up = (camera.getVertical());
  gluLookAt(eye.x, eye.y, eye.z,
            at.x, at.y, at.z,
            up.x, up.y, up.z);

  draw();
  
  //glFlush();
  glFinish();
  glutSwapBuffers();
}

// On reshape, constructs a camera that perfectly fits the window.
void reshape(GLint w, GLint h) {
  glViewport(10, 10, w-20, h-20); // 10px border
  camera.rePerspect();
}

// Requests to draw the next frame.

void timer(int v) {
  glutPostRedisplay();
  glutTimerFunc(1000/60, timer, v);
  frame_count += 1;
}

// Moves the camera according to the key pressed, then ask to refresh the
// display.
void specialkey_callback(int key, int, int) {
  switch (key) {
    case GLUT_KEY_LEFT: camera.yaw(0.02); break;
    case GLUT_KEY_RIGHT: camera.yaw(-0.02); break;
    case GLUT_KEY_UP: camera.pitch(0.02); break;
    case GLUT_KEY_DOWN: camera.pitch(-0.02); break;
    case GLUT_KEY_PAGE_UP: camera.roll(-0.02); break;
    case GLUT_KEY_PAGE_DOWN: camera.roll(0.02); break;
  }
  glutPostRedisplay();
}

int exit_simulator() {
  glutDestroyWindow (window_main);
  exit(0);
  return 0;
}

void keyboard_callback(unsigned char key, int x, int y){

  {    } if(key=='w'){ // forward
    camera.fly(camera.forwardVec());
  } else if(key=='s'){ // back
    camera.fly(-camera.forwardVec());
  } else if(key=='a'){ // left
    camera.fly(-camera.rightVec());
  } else if(key=='d'){ //right
    camera.fly(camera.rightVec());
  } else if(key=='e'){ // Ascend
    camera.fly(camera.upVec());
  } else if (key=='z') { // Descend
    camera.fly(-camera.upVec());
  } else if (key=='\e') { // if Escape
    exit_simulator();
  }
  glutPostRedisplay();
}

int truncate_almost_zero(int val){
  return (abs(val)<=1)? 0 : val;
}

#include<iostream>

void mouse_move_callback(int x, int y) {
  if(mouse_moving){
    //mouse_x_delta = truncate_almost_zero(x - mouse_x_ago);
    //mouse_y_delta = truncate_almost_zero(y - mouse_y_ago);
    mouse_x_delta = x - mouse_x_ago;
    mouse_y_delta = y - mouse_y_ago;

    GLint w = glutGet(GLUT_WINDOW_WIDTH);
    GLint h = glutGet(GLUT_WINDOW_HEIGHT);

    float dtheta_x =
      - (double)mouse_x_delta/w * glm::radians(camera.thetaFovy()*(double)w/h)/M_2_PI;
    camera.yaw(dtheta_x);

    float dtheta_y =
      - (double)mouse_y_delta/h * glm::radians(camera.thetaFovy()*(double)h/h)/M_2_PI;
    camera.pitch(dtheta_y);

  }

  // renew
  mouse_x_ago = x;
  mouse_y_ago = y;
}

void mouse_button_callback(int button, int state, int x, int y){
  // GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, or GLUT_RIGHT_BUTTON
  if (state==GLUT_DOWN) {
    mouse_x_start = x;
    mouse_y_start = y;
    mouse_x_ago = x;
    mouse_y_ago = y;
    mouse_moving = true;
  } else if (state==GLUT_UP) {
    mouse_moving = false;
  }
}

void mouse_wheel_callback(int button, int dir, int x, int y){
  if (dir > 0) {
    camera.fovyZoom(+1.00);
  } else {
    camera.fovyZoom(-1.00);
  }
}

// Initializes GLUT and enters the main loop.
int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowPosition(80, 80);
  glutInitWindowSize(800, 600);
  int window_main = glutCreateWindow("Bouncing Balls");
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  //
  glutSpecialFunc(specialkey_callback);
  glutKeyboardFunc(keyboard_callback);
  glutMotionFunc(mouse_move_callback); // mouse move if pressed
  glutPassiveMotionFunc(mouse_move_callback); // mouse move if not pressed
  glutMouseFunc(mouse_button_callback);
  glutMouseWheelFunc(mouse_wheel_callback);
  //
  glutTimerFunc(100, timer, 0);
  init();
  glutMainLoop();
}

#endif
