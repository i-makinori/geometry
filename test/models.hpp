
#ifndef _MODELS_H_
#define _MODELS_H_

#include "utils.hpp"
#include "transformation.hpp"
#include "camera.hpp"

// Colors
GLfloat WHITE[] = {1, 1, 1};
GLfloat RED[] = {1, 0, 0};
GLfloat GREEN[] = {0, 1, 0};
GLfloat MAGENTA[] = {1, 0, 1};


// A ball.  A ball has a radius, a color, and bounces up and down between
// a maximum height and the xz plane.  Therefore its x and z coordinates
// are fixed.  It uses a lame bouncing algorithm, simply moving up or
// down by 0.05 units at each frame.
class Ball {
  double radius;
  GLfloat* color;
  double maximumHeight;
  double x;
  double y;
  double z;
  int direction;
public:
  Ball(double r, GLfloat* c, double h, double x, double z):
      radius(r), color(c), maximumHeight(h), direction(-1),
      y(h), x(x), z(z) {
  }
  void update() {
    y += direction * 0.05;
    if (y > maximumHeight) {
      y = maximumHeight; direction = -1;
    } else if (y < radius) {
      y = radius; direction = 1;
    }
    glPushMatrix();
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
    glTranslated(x, y, z);
    glutSolidSphere(radius, 30, 30);
    glPopMatrix();
  }
  void draw(){
  }
  
};


// A checkerboard class.  A checkerboard has alternating red and white
// squares.  The number of squares is set in the constructor.  Each square
// is 1 x 1.  One corner of the board is (0, 0) and the board stretches out
// along positive x and positive z.  It rests on the xz plane.  I put a
// spotlight at (4, 3, 7).
class Checkerboard {
  int displayListId;
  int width;
  int depth;
public:
  Checkerboard(int width, int depth): width(width), depth(depth) {}
  double centerx() {return width / 2;}
  double centerz() {return depth / 2;}
  void create() {
    displayListId = glGenLists(1);
    glNewList(displayListId, GL_COMPILE);
    GLfloat lightPosition[] = {4, 3, 7, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glBegin(GL_QUADS);

    glNormal3d(0, 1, 0);
    for (int x = 0; x < width - 1; x++) {
      for (int z = 0; z < depth - 1; z++) {
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE,
                     (x + z) % 2 == 0 ? RED : WHITE);
        glVertex3d(x, 0, z);
        glVertex3d(x+1, 0, z);
        glVertex3d(x+1, 0, z+1);
        glVertex3d(x, 0, z+1);
      }
    }
    glEnd();
    glEndList();
  }
  void draw() {
    glCallList(displayListId);
  }
};

void draw_axes(double length) {
  // axes are ten units long.
  glDisable(GL_LIGHTING);
  glBegin(GL_LINES); {
    glColor3f(1, 0, 0); glVertex3f(0, 0, 0); glVertex3f(length, 0, 0);
    glColor3f(0, 1, 0); glVertex3f(0, 0, 0); glVertex3f(0, length, 0);
    glColor3f(0, 0, 1); glVertex3f(0, 0, 0); glVertex3f(0, 0, length);
  } glEnd();
  glEnable(GL_LIGHTING);
}

void draw_2d_rect(float x1, float y1, float x2, float y2){
  GLint w = glutGet(GLUT_WINDOW_WIDTH);
  GLint h = glutGet(GLUT_WINDOW_HEIGHT);
  
  push_to_screen_procedures(h, w); {
    glBegin(GL_POLYGON);
    glVertex2f(x1, y1);
    glVertex2f(x1, y2);
    glVertex2f(x2, y2);
    glVertex2f(x2, y1);
    glEnd();
  } pop_from_screen_procedures();
}

void draw_status_texts(Camera camera, int frame_count){

  GLint w = glutGet(GLUT_WINDOW_WIDTH);
  GLint h = glutGet(GLUT_WINDOW_HEIGHT);

  
  glm::vec4 camera_dir = camera.getDirection();
  std::string text;
  text =
    "dirx:" + std::to_string(  camera_dir.x) +
    ", diry:" + std::to_string(camera_dir.y) +
    ", dirz:" + std::to_string(camera_dir.z);
  DrawString(text, w, h ,100,400);

  glm::vec4 at = (camera.lookAt());
  text =
    "atx:" + std::to_string(  at.x) +
    ", aty:" + std::to_string(at.y) +
    ", atz:" + std::to_string(at.z);
  DrawString(text, w, h ,100,500);
  
  glm::vec4 camera_xyz = camera.getPosition();
  text =
    "posx:"   + std::to_string(camera_xyz.x) +
    ", posy:" + std::to_string(camera_xyz.y) +
    ", posz:" + std::to_string(camera_xyz.z);
  DrawString(text, w, h ,100,525);
  
  text =
    "count:" + std::to_string(frame_count);
  DrawString(text, w, h ,100,550);
}

#endif
