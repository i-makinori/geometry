;; self written algebra, tensor library

;; References
;; 線型代数 佐武
;; Prologでつくる
;; on Lisp
;; Let Over Lambda
;; SICP
;; 多様体入門


(defun matrix (n m &body params)
  (make-array (list n m)
              :initial-value 0.00))

(defun vector ())

(defun add ())

(defun sub ())

(defun product ())

(defun dot ())

(defun hoge ()
  ;; 行列式
)

(defun cross ())


(defun normalize ())


(defun magunitude ())
