

# 幾何学サンプル

### Geometry and Algebra

- [佐武一郎, 線形代数学(新装版), 裳華房, 2015]
- [回転ベクトル・回転行列・クォータニオン・オイラー角についてまとめてみた - かみのメモ](https://kamino.hatenablog.com/entry/rotation_expressions#sec1)
- [3次元の回転行列 | 理系のための備忘録](https://science-log.com/%E6%95%B0%E5%AD%A6/3%E6%AC%A1%E5%85%83%E3%81%AE%E5%9B%9E%E8%BB%A2%E8%A1%8C%E5%88%97/)
- [回転行列の表現方法](http://www.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/tech0007.html)
- [コンピュータグラフィックス基礎](http://www.cgg.cs.tsukuba.ac.jp/~endo/lecture/2020/cgbasics/03/03.pdf)
- [コンピューターグラフィックスＳ](http://www.cg.ces.kyutech.ac.jp/lecture/cg/cg09_transformation2.pdf)
[Calculating the gluPerspective matrix and other OpenGL matrix maths | Unspecified Behaviour](https://unspecified.wordpress.com/2012/06/21/calculating-the-gluperspective-matrix-and-other-opengl-matrix-maths/)

### Open GL

- [Viewing and Modeling Transformations (OpenGL Programming) Part 2](http://what-when-how.com/opengl-programming-guide/viewing-and-modeling-transformations-opengl-programming-part-2/)
- [OpenGL Examples](https://cs.lmu.edu/~ray/notes/openglexamples/)
- [OpenGL Matrix Class](http://www.songho.ca/opengl/gl_matrix.html)
- [gl_02.ppt - tmppx0bfwgf_gl_02.pdf](https://cs.brynmawr.edu/Courses/cs312/fall2010/lectures/gl_02.pdf)
- [An OpenGL Flight Simulator](https://cs.lmu.edu/~ray/notes/flightsimulator/)

### lisp

- [GitHub - 3b/cl-opengl: cl-opengl is a set of CFFI bindings to the OpenGL, GLU and GLUT APIs.](https://github.com/3b/cl-opengl)
- [letter: cl-opengl で文字を表示するときに便利な opengl-text](http://read-eval-print.blogspot.com/2009/04/cl-opengl-opengl-text.html)
